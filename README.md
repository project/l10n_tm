Preparation of migrating Drupal UI localization into Translation Memory (TM)

This project is not a regular module or theme for Drupal. Instead, it's special purpose is to provide further contexts of UI strings helping translators working on localize.drupal.org.

With more grouping options available we can prepare easier for integration with/migration into a Translation Memory (TM) tool hopefully happening in the future.
