Generating a dictionary based on multiple locales' collection found available.

Strings collected into separate lists in the following order:

01 singlewordeds (lower case)                      singular
02 singlewordeds (lower case)                      plural
03 singlewordeds (lower case)                      with plural form

04 Singlewordeds (upper case)                      singular
05 Singlewordeds (upper case)                      plural
06 Singlewordeds (upper case)                      with plural form

07 two wordeds   (both lower case)                 singular
08 two wordeds   (both lower case)                 plural
09 two wordeds   (both lower case)                 with plural form

10 Two wordeds   (first upper, then lower case)    singular
11 Two wordeds   (first upper, then lower case)    plural
12 Two wordeds   (first upper, then lower case)    with plural form

13 Two Wordeds   (both upper case)                 singular
14 Two Wordeds   (both upper case)                 plural
15 Two Wordeds   (both upper case)                 with plural form

16 two Wordeds   (first lower, then upper case)    singular
17 two Wordeds   (first lower, then upper case)    plural
18 two Wordeds   (first lower, then upper case)    with plural form
